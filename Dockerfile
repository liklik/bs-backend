FROM java:8
EXPOSE 8080 8888

VOLUME /tmp
ADD target/bs-backend.jar  /app.jar
RUN bash -c 'touch /app.jar'
#ENTRYPOINT ["java","-jar","/app.jar"]
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]