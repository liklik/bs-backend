package cn.xing.common.constant;

public class SysConfigConstans {
    public static final String SYS_AUTO_ACCEPT_ORDER="sys_auto_accept_order";
    public static final String SYS_AUTO_REFUND_WHEN_NEW="sys_auto_refund_when_new";
    public static final String SYS_AUDIO_REPORT="sys_audio_report";
}
