package cn.xing.common.constant;

public class SocketIOConstans {
    /**
     * 自定义事件`push_data_event`,用于服务端与客户端通信
     */
    public static final String LOGIN_EVENT = "login";
    /**
     * 有新的已支付订单
     */
    public static final String NEW_PAYED_ORDER = "newPayedOrder";
    // public static final String  USER_CANCEL_ORDER= "userCancelOrder";
    public static final String ORDER_STATUS_CHANGE = "orderStatusChange";
    public static final String AFTER_SALE_STATUS_CHANGE = "afterSaleStatusChange";
    public static final String PUSH_DATA_EVENT = "serverPush";
    public static final String SYS_AUTO_ACCEPT_ORDER="sys_auto_accept_order";

}
