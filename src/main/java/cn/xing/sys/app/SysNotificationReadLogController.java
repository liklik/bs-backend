package cn.xing.sys.app;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.xing.modules.app.entity.SysNotificationReadLogEntity;
import cn.xing.modules.app.service.SysNotificationReadLogService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 系统通知已读记录
 *
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
@RestController
@RequestMapping("sys/sysnotificationreadlog")
public class SysNotificationReadLogController {
    @Autowired
    private SysNotificationReadLogService sysNotificationReadLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("sys:sysnotificationreadlog:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysNotificationReadLogService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("sys:sysnotificationreadlog:info")
    public R info(@PathVariable("id") Long id){
		SysNotificationReadLogEntity sysNotificationReadLog = sysNotificationReadLogService.getById(id);

        return R.ok().put("sysNotificationReadLog", sysNotificationReadLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("sys:sysnotificationreadlog:save")
    public R save(@RequestBody SysNotificationReadLogEntity sysNotificationReadLog){
		sysNotificationReadLogService.save(sysNotificationReadLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("sys:sysnotificationreadlog:update")
    public R update(@RequestBody SysNotificationReadLogEntity sysNotificationReadLog){
		sysNotificationReadLogService.updateById(sysNotificationReadLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("sys:sysnotificationreadlog:delete")
    public R delete(@RequestBody Long[] ids){
		sysNotificationReadLogService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
