package cn.xing.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "alibaba.sms")
@Configuration
@Data
public class SmsConfig {
    private String accessKey;
    private String accessSecret;
}
