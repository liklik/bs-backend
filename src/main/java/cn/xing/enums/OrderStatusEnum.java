package cn.xing.enums;


import org.springframework.lang.Nullable;

public enum OrderStatusEnum {
    // IN_AFTER_SALE(-11,"售后中"),
    REFUNDED(-3, "已退款"),
    // 等待商家确认退款
    BUS_DEALING(-2, "商家处理中"),
    CANCEL(-1, "取消"),
    NEW(0, "待付款"),
    PAYED(1, "已付款"),
    TAKEN(2, "商家已接单"),
    SEND_OUT(3, "已送出"),
    REACH(4, "已送达");


    /**
     * 编号
     */
    private Integer code;
    /**
     * 描述
     */
    private String description;

    OrderStatusEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }


    /**
     * 通过code取枚举
     *
     * @return
     */
    public static OrderStatusEnum getTypeByValue(Integer code) {
        if (code == null) {
            return null;
        }

        for (OrderStatusEnum enums : OrderStatusEnum.values()) {
            if (enums.getCode().equals(code)) {
                return enums;
            }
        }
        return null;
    }

    /**
     * 通过code取描述
     *
     * @return
     */
    public static String getDescByCode(@Nullable Integer code) {
        String description = "";
        if (code != null) {
            OrderStatusEnum type = getTypeByValue(code);
            if (type != null) {
                description = type.getDescription();
            }
        }
        return description;
    }
}
