package cn.xing.enums;

public enum ChartTimeUnitEnum {
    YEAR("year","年"),
    MONTH("month","月"),
    DAY("day","日");


    /**
     * 编号
     */
    private String timeUnit;
    /**
     * 描述
     */
    private String description;

     ChartTimeUnitEnum(String timeUnit, String description) {
        this.timeUnit = timeUnit;
        this.description = description;
    }

    public String getTimeUnit() {
        return timeUnit;
    }

    public String getDescription() {
        return description;
    }

    /**
     * 通过code取枚举
     *
     * @return
     */
    public static ChartTimeUnitEnum getTypeByValue(String timeUnit) {
        if (timeUnit == null) {
            return null;
        }

        for (ChartTimeUnitEnum enums : ChartTimeUnitEnum.values()) {
            if (enums.getTimeUnit().equals(timeUnit)) {
                return enums;
            }
        }
        return null;
    }

    /**
     * 通过code取描述
     *
     * @return
     */
    public static String getDescByCode(String timeUnit) {
        String description = "";
        if (timeUnit != null) {
            ChartTimeUnitEnum type = getTypeByValue(timeUnit);
            if (type != null) {
                description = type.getDescription();
            }
        }
        return description;
    }
}
