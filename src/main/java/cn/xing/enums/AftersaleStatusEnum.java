package cn.xing.enums;

public enum AftersaleStatusEnum {
    BUSINESS_REFUSE(-1,"商家拒绝"),
    NEW(0,"用户提交"),
    BUSINESS_CONFIRM(1,"商家接受退款"),
    REFUNDED(2,"退款到账");


    /**
     * 编号
     */
    private Integer code;
    /**
     * 描述
     */
    private String description;

     AftersaleStatusEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    /**
     * 通过code取枚举
     *
     * @return
     */
    public static AftersaleStatusEnum getTypeByValue(Integer code) {
        if (code == null) {
            return null;
        }

        for (AftersaleStatusEnum enums : AftersaleStatusEnum.values()) {
            if (enums.getCode().equals(code)) {
                return enums;
            }
        }
        return null;
    }

    /**
     * 通过code取描述
     *
     * @return
     */
    public static String getDescByCode(Integer code) {
        String description = "";
        if (code != null) {
            AftersaleStatusEnum type = getTypeByValue(code);
            if (type != null) {
                description = type.getDescription();
            }
        }
        return description;
    }
}
