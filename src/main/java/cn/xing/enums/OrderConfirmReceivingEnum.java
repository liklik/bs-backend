package cn.xing.enums;

public enum OrderConfirmReceivingEnum {
    NO(0,"等待确认收货"),
    YES(1,"已确认收货");


    /**
     * 编号
     */
    private Integer code;
    /**
     * 描述
     */
    private String description;

     OrderConfirmReceivingEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    /**
     * 通过code取枚举
     *
     * @return
     */
    public static OrderConfirmReceivingEnum getTypeByValue(Integer code) {
        if (code == null) {
            return null;
        }

        for (OrderConfirmReceivingEnum enums : OrderConfirmReceivingEnum.values()) {
            if (enums.getCode().equals(code)) {
                return enums;
            }
        }
        return null;
    }

    /**
     * 通过code取描述
     *
     * @return
     */
    public static String getDescByCode(Integer code) {
        String description = "";
        if (code != null) {
            OrderConfirmReceivingEnum type = getTypeByValue(code);
            if (type != null) {
                description = type.getDescription();
            }
        }
        return description;
    }
}
