package cn.xing.enums;

public enum OrderRefundEnum {
    NO(0,"未退款"),
    YES(1,"已退款");


    /**
     * 编号
     */
    private Integer code;
    /**
     * 描述
     */
    private String description;

     OrderRefundEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    /**
     * 通过code取枚举
     *
     * @return
     */
    public static OrderRefundEnum getTypeByValue(Integer code) {
        if (code == null) {
            return null;
        }

        for (OrderRefundEnum enums : OrderRefundEnum.values()) {
            if (enums.getCode().equals(code)) {
                return enums;
            }
        }
        return null;
    }

    /**
     * 通过code取描述
     *
     * @return
     */
    public static String getDescByCode(Integer code) {
        String description = "";
        if (code != null) {
            OrderRefundEnum type = getTypeByValue(code);
            if (type != null) {
                description = type.getDescription();
            }
        }
        return description;
    }
}
