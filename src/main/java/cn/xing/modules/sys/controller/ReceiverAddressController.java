package cn.xing.modules.sys.controller;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.app.annotation.LoginUser;
import cn.xing.modules.app.entity.ReceiverAddressEntity;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.app.service.ReceiverAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;


/**
 * 收货地址
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("receiveraddress")
@Api("收货地址")
public class ReceiverAddressController {
    @Autowired
    private ReceiverAddressService receiverAddressService;


    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("modules:receiveraddress:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = receiverAddressService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("modules:receiveraddress:info")
    public R info(@PathVariable("id") Long id) {
        ReceiverAddressEntity receiverAddress = receiverAddressService.getById(id);

        return R.ok().put("receiverAddress", receiverAddress);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("modules:receiveraddress:save")
    public R save(@RequestBody ReceiverAddressEntity receiverAddress) {
        receiverAddressService.save(receiverAddress);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("modules:receiveraddress:update")
    public R update(@RequestBody ReceiverAddressEntity receiverAddress) {
        receiverAddressService.updateById(receiverAddress);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:receiveraddress:delete")
    public R delete(@RequestBody Long[] ids) {
        receiverAddressService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
