package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.xing.modules.sys.entity.AfterSaleEntity;
import cn.xing.modules.sys.service.AfterSaleService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 售后记录
 *
 * @author
 * @email 
 * @date 2020-11-02 18:54:17
 */
@RestController
@RequestMapping("sys/aftersale")
public class AfterSaleController {
    @Autowired
    private AfterSaleService afterSaleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("sys:aftersale:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = afterSaleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("sys:aftersale:info")
    public R info(@PathVariable("id") Long id){
		AfterSaleEntity afterSale = afterSaleService.getById(id);

        return R.ok().put("afterSale", afterSale);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("sys:aftersale:save")
    public R save(@RequestBody AfterSaleEntity afterSale){
		afterSaleService.save(afterSale);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("sys:aftersale:update")
    public R update(@RequestBody AfterSaleEntity afterSale){
		afterSaleService.updateById(afterSale);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("sys:aftersale:delete")
    public R delete(@RequestBody Long[] ids){
		afterSaleService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 拒绝 售后
     */
    @PutMapping("/{id}/refuse")
    // @RequiresPermissions("sys:aftersale:delete")
    public R refuse(@PathVariable("id") Long id){
        afterSaleService.refuse(id);

        return R.ok();
    }

    /**
     * 确认 售后-退款
     */
    @PutMapping("/{id}/confirm")
    // @RequiresPermissions("sys:aftersale:delete")
    public R confirm(@PathVariable("id") Long id){
        afterSaleService.confirm(id);

        return R.ok();
    }
}
