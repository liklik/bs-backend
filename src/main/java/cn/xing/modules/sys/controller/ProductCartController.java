package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.xing.modules.app.entity.ProductCartEntity;
import cn.xing.modules.app.service.ProductCartService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 购物车
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("productcart")
public class ProductCartController {
    @Autowired
    private ProductCartService productCartService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("modules:productcart:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = productCartService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("modules:productcart:info")
    public R info(@PathVariable("id") Long id){
		ProductCartEntity productCart = productCartService.getById(id);

        return R.ok().put("productCart", productCart);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("modules:productcart:save")
    public R save(@RequestBody ProductCartEntity productCart){
		productCartService.save(productCart);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("modules:productcart:update")
    public R update(@RequestBody ProductCartEntity productCart){
		productCartService.updateById(productCart);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:productcart:delete")
    public R delete(@RequestBody Long[] ids){
		productCartService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
