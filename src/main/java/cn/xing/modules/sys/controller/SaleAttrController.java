package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.xing.modules.app.entity.SaleAttrEntity;
import cn.xing.modules.app.service.SaleAttrService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 销售属性
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("/saleattr")
public class SaleAttrController {
    @Autowired
    private SaleAttrService saleAttrService;

    // @ApiOperation("根据销售属性名，查询 销售属性")
    @GetMapping("/getbyname")
    // @RequiresPermissions(":productsaleattrrelation:list")
    public R getByName(String name){
        List<SaleAttrEntity> list =  saleAttrService.getByName(name);

        return R.ok().put("list", list);
    }

    // @ApiOperation("查询所有销售属性名")
    @GetMapping("/all/name")
    // @RequiresPermissions(":productsaleattrrelation:list")
    public R all(@RequestParam(value = "key",required = false,defaultValue = "") String key){
        List<String> list =  saleAttrService.queryDistinctName(key);

        return R.ok().put("list", list);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("modules:saleattr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = saleAttrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("modules:saleattr:info")
    public R info(@PathVariable("id") Long id){
		SaleAttrEntity saleAttr = saleAttrService.getById(id);

        return R.ok().put("saleAttr", saleAttr);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("modules:saleattr:save")
    public R save(@RequestBody SaleAttrEntity saleAttr){
		saleAttrService.save(saleAttr);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("modules:saleattr:update")
    public R update(@RequestBody SaleAttrEntity saleAttr){
		saleAttrService.updateById(saleAttr);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:saleattr:delete")
    public R delete(@RequestBody Long[] ids){
		saleAttrService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
