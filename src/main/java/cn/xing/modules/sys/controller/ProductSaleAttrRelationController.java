package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.xing.modules.app.entity.ProductSaleAttrRelationEntity;
import cn.xing.modules.app.service.ProductSaleAttrRelationService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 商品-销售属性关系
 *
 * @author
 * @email
 * @date 2020-10-24 21:02:17
 */
@RestController
@RequestMapping("/productsaleattrrelation")
@Api("商品-销售属性关系")
public class ProductSaleAttrRelationController {
    @Autowired
    private ProductSaleAttrRelationService productSaleAttrRelationService;




    /**
    //  * 根据销售属性名，查询 销售属性
    //  */
    // @ApiOperation("查询所有销售属性名")
    // @GetMapping("/all")
    // // @RequiresPermissions(":productsaleattrrelation:list")
    // public R getByName(String name){
    //     List<Prod> list =  productSaleAttrRelationService.queryByName();
    //
    //     return R.ok().put("list", list);
    // }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions(":productsaleattrrelation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = productSaleAttrRelationService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/info")
    public R queryByProductIdAndSaleAttrId(Long productId,Long saleAttrId){
        ProductSaleAttrRelationEntity productSaleAttrRelation = productSaleAttrRelationService.queryByProductIdAndSaleAttrId(productId,saleAttrId);

        return R.ok().put("data", productSaleAttrRelation);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions(":productsaleattrrelation:info")
    public R info(@PathVariable("id") Long id){
		ProductSaleAttrRelationEntity productSaleAttrRelation = productSaleAttrRelationService.getById(id);

        return R.ok().put("productSaleAttrRelation", productSaleAttrRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions(":productsaleattrrelation:save")
    public R save(@RequestBody ProductSaleAttrRelationEntity productSaleAttrRelation){

		productSaleAttrRelationService.saveByProductIdAndSaleAttrId(productSaleAttrRelation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions(":productsaleattrrelation:update")
    public R update(@RequestBody ProductSaleAttrRelationEntity productSaleAttrRelation){
		productSaleAttrRelationService.updateById(productSaleAttrRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions(":productsaleattrrelation:delete")
    public R delete(@RequestBody Long[] ids){
		productSaleAttrRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
