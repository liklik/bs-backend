package cn.xing.modules.sys.controller;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;
import cn.xing.modules.app.entity.CommentEntity;
import cn.xing.modules.app.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;


/**
 * 评论
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    /**
     * 列表
     */
    @RequestMapping("/all")
    // @RequiresPermissions("modules:comment:list")
    public R all(String orderSn) {
        CommentEntity commentEntity = commentService.queryByOrderSn(orderSn);

        return R.ok().put("data", commentEntity);
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("modules:comment:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = commentService.queryPage("", params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("modules:comment:info")
    public R info(@PathVariable("id") Long id) {
        CommentEntity comment = commentService.getById(id);

        return R.ok().put("comment", comment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("modules:comment:save")
    public R save(@RequestBody CommentEntity comment) {
        commentService.save(comment);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("modules:comment:update")
    public R update(@RequestBody CommentEntity comment) {
        commentService.updateById(comment);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:comment:delete")
    public R delete(@RequestBody Long[] ids) {
        commentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
