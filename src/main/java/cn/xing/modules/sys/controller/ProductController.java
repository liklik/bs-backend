package cn.xing.modules.sys.controller;

import cn.xing.common.annotation.SysLog;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.app.entity.ProductEntity;
import cn.xing.modules.app.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;


/**
 * 商品
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("product")
@Api(description = "APP 商品接口")
public class ProductController {
    @Autowired
    private ProductService productService;

    /**
     * 列表
     */
    @ApiOperation("获取商品分页（包含销售属性），无需登录")
    @GetMapping("/listwithsaleattr")
    // @RequiresPermissions("modules:product:list")
    public R listWithSaleAttr(@RequestParam Map<String, Object> params) {
        PageUtils page = productService.queryPageWithSaleAttr(params);

        return R.ok().put("page", page);
    }

    /**
     * 所有
     * @return
     */
    @GetMapping("/all")
    // @RequiresPermissions("modules:product:list")
    public R all() {
        List<ProductEntity> list = productService.list();

        return R.ok().put("list", list);
    }

    /**
     * 列表
     */
    // @ApiOperation("获取商品分页，无需登录")
    @GetMapping("/list")
    // @RequiresPermissions("modules:product:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = productService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("根据id获取商品详情，无需登录")
    @GetMapping("/info/{id}")
    // @RequiresPermissions("modules:product:info")
    public R info(@PathVariable("id") Long id) {
        ProductEntity product = productService.getById(id);

        return R.ok().put("product", product);
    }

    /**
     * 保存
     */
    @SysLog("添加商品")
    @RequestMapping("/save")
    // @RequiresPermissions("modules:product:save")
    public R save(@RequestBody ProductEntity product) {
        productService.save(product);

        return R.ok();
    }

    /**
     * 修改
     */
    @SysLog("修改商品")
    @RequestMapping("/update")
    // @RequiresPermissions("modules:product:update")
    public R update(@RequestBody ProductEntity product) {
        productService.updateById(product);

        return R.ok();
    }

    /**
     * 删除
     */
    @SysLog("删除商品")
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:product:delete")
    public R delete(@RequestBody Long[] ids) {
        productService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
