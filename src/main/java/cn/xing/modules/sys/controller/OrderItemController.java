package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.xing.modules.app.entity.OrderItemEntity;
import cn.xing.modules.app.service.OrderItemService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * 订单项
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("orderitem")
public class OrderItemController {
    @Autowired
    private OrderItemService orderItemService;

    /**
     * 列表
     */
    @RequestMapping("/{orderSn}/all")
    // @RequiresPermissions("modules:orderitem:list")
    public R list(@PathVariable("orderSn") String orderSn){
        List<OrderItemEntity> list = orderItemService.queryByOrderSn(orderSn);

        return R.ok().put("data", list);
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("modules:orderitem:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = orderItemService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("modules:orderitem:info")
    public R info(@PathVariable("id") Integer id){
		OrderItemEntity orderItem = orderItemService.getById(id);

        return R.ok().put("orderItem", orderItem);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("modules:orderitem:save")
    public R save(@RequestBody OrderItemEntity orderItem){
		orderItemService.save(orderItem);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("modules:orderitem:update")
    public R update(@RequestBody OrderItemEntity orderItem){
		orderItemService.updateById(orderItem);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:orderitem:delete")
    public R delete(@RequestBody Integer[] ids){
		orderItemService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
