package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import cn.xing.common.annotation.SysLog;
import cn.xing.common.utils.ShiroUtils;
import cn.xing.modules.app.annotation.LoginUser;
import cn.xing.modules.sys.entity.SysUserEntity;
import com.alipay.api.AlipayApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.xing.modules.app.entity.OrderEntity;
import cn.xing.modules.app.service.OrderService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 订单
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("order")
public class OrderController {
    @Autowired
    private OrderService orderService;



    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("modules:order:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = orderService.queryPage(params);

        return R.ok().put("page", page);
    }

    @SysLog("取消订单")
    @PostMapping("/{id}/cancel")
    public R managerCancelOrder(@PathVariable Long id){
        orderService.managerCancelOrder(id,ShiroUtils.getUserId());

        return R.ok();
    }

    @SysLog("接单")
    @PostMapping("/{id}/accept")
    public R managerAcceptOrder( @PathVariable Long id){
        orderService.managerAcceptOrder(id, ShiroUtils.getUserId());

        return R.ok();
    }

    @SysLog("拒绝接单")
    @PostMapping("/{id}/refuse")
    public R managerRefuseOrder(@PathVariable Long id){
        orderService.managerRefuseOrder(id,ShiroUtils.getUserId());

        return R.ok();
    }

    @SysLog("配送订单")
    @PostMapping("/{id}/delivery")
    public R managerDeliveryOrder(@PathVariable Long id){
        orderService.managerDeliveryOrder(id,ShiroUtils.getUserId());

        return R.ok();
    }

    @SysLog("确认送达订单")
    @PostMapping("/{id}/confirmsended")
    public R managerConfirmSendedOrder(@PathVariable Long id){
        orderService.managerConfirmSendedOrder(id,ShiroUtils.getUserId());

        return R.ok();
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("modules:order:info")
    public R info(@PathVariable("id") Long id){
		OrderEntity order = orderService.getById(id);

        return R.ok().put("order", order);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("modules:order:save")
    public R save(@RequestBody OrderEntity order){
		orderService.save(order);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("modules:order:update")
    public R update(@RequestBody OrderEntity order){
		orderService.updateById(order);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:order:delete")
    public R delete(@RequestBody Long[] ids){
		orderService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    @GetMapping("{orderSn}/refund")
    public R refund(@PathVariable String orderSn,String reason) throws AlipayApiException {
        orderService.doRefundAndMark(orderSn,reason);
        return R.ok();
    }
}
