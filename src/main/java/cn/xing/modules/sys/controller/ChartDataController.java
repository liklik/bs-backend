package cn.xing.modules.sys.controller;

import cn.xing.common.exception.RRException;
import cn.xing.common.utils.R;
import cn.xing.enums.ChartTimeUnitEnum;
import cn.xing.modules.sys.service.ChartService;
import cn.xing.modules.sys.vo.OrderCountLineVo;
import cn.xing.modules.sys.vo.SaleCountPieVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("sys/chart")
public class ChartDataController {

    @Autowired
    private ChartService chartService;

    @GetMapping("/pie/salecount")
    public R pie(String startTime,
                 String endTime) {
        List<SaleCountPieVo> list = chartService.querySaleCountPieDataList(startTime,endTime);
        return R.ok().put("data", list);
    }

    @GetMapping("/line/union/{timeUnit}")
    public R lineUnionData(@PathVariable("timeUnit") String timeUnit,
                           String startTime,
                           String endTime) {
        ChartTimeUnitEnum timeUnitEnum = ChartTimeUnitEnum.getTypeByValue(timeUnit);
        if (timeUnitEnum == null) {
            throw new RRException("暂时只支持day、month的方式");
        }
        OrderCountLineVo orderCountLineVo = chartService.queryLineUnion(timeUnitEnum,startTime,endTime);
        return R.ok().put("data", orderCountLineVo);
    }
}
