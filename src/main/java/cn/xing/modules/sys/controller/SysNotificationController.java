package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.xing.modules.app.entity.SysNotificationEntity;
import cn.xing.modules.app.service.SysNotificationService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 系统通知
 *
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
@RestController
@RequestMapping("sys/sysnotification")
public class SysNotificationController {
    @Autowired
    private SysNotificationService sysNotificationService;

    @GetMapping("/all/mine/unread")
    public R listMyUnread(){
        List<SysNotificationEntity> list = sysNotificationService.listMyUnread();
        return R.ok().put("data", list);
    }

    @PutMapping("/all/allread")
    public R markAllRead(){
        sysNotificationService.markAllRead();
        return R.ok();
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("sys:sysnotification:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysNotificationService.queryPageUserUnread(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息，并标记为已读
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("sys:sysnotification:info")
    public R info(@PathVariable("id") Long id){
		SysNotificationEntity sysNotification = sysNotificationService.getAndMarkReaded(id);

        return R.ok().put("sysNotification", sysNotification);
    }


}
