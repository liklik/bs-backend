package cn.xing.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.xing.modules.app.entity.DeliveryLogEntity;
import cn.xing.modules.app.service.DeliveryLogService;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;



/**
 * 配送记录
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@RestController
@RequestMapping("deliverylog")
public class DeliveryLogController {
    @Autowired
    private DeliveryLogService deliveryLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("modules:deliverylog:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = deliveryLogService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("modules:deliverylog:info")
    public R info(@PathVariable("id") Long id){
		DeliveryLogEntity deliveryLog = deliveryLogService.getById(id);

        return R.ok().put("deliveryLog", deliveryLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("modules:deliverylog:save")
    public R save(@RequestBody DeliveryLogEntity deliveryLog){
		deliveryLogService.save(deliveryLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("modules:deliverylog:update")
    public R update(@RequestBody DeliveryLogEntity deliveryLog){
		deliveryLogService.updateById(deliveryLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("modules:deliverylog:delete")
    public R delete(@RequestBody Long[] ids){
		deliveryLogService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
