package cn.xing.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 售后记录
 * 
 * @author 
 * @email 
 * @date 2020-11-02 18:54:17
 */
@Data
@TableName("tb_after_sale")
public class AfterSaleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 订单号
	 */
	private String orderSn;
	/**
	 * 用户-原因
	 */
	private String reason;
	/**
	 * 图片数组，以;分割
	 */
	private String imgUrls;
	/**
	 * 售后状态：-1:商家拒绝;0:用户提交;1:商家接受退款;2:退款到账
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
