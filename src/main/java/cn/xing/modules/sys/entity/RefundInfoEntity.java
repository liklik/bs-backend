package cn.xing.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 退款信息
 * 
 * @author 
 * @email 
 * @date 2020-11-02 18:54:17
 */
@Data
@TableName("tb_refund_info")
public class RefundInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 订单号
	 */
	private String orderSn;
	/**
	 * 退款人民币
	 */
	private BigDecimal refundFee;
	/**
	 * 退款流水号
	 */
	private String refundSn;
	/**
	 * 退款状态 0:未成功 1:已成功
	 */
	private Integer refundStatus;
	/**
	 * 退款原因
	 */
	private String reason;
	/**
	 * 支付宝退款失败原因
	 */
	private String failedReason;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
