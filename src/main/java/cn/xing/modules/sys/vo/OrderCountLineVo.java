package cn.xing.modules.sys.vo;

import lombok.Data;

import java.util.List;

@Data
public class OrderCountLineVo {
    private List<OrderCountVo> orderCountVoList;
}
