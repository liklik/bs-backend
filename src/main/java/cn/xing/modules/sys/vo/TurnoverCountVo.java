package cn.xing.modules.sys.vo;

import lombok.Data;

/**
 * 营业额
 */
@Data
public  class TurnoverCountVo {
    /**
     * 日期
     */
    private String date;
    /**
     * 订单统计
     */
    private int orderCount;
}
