package cn.xing.modules.sys.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public  class OrderCountVo{
    /**
     * 日期
     */
    private String date;
    /**
     * 订单数量统计
     */
    private int orderCount;

    /**
     * 营业额 统计
     */
    private BigDecimal turnoverCount;
}
