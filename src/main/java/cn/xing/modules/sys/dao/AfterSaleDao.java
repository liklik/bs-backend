package cn.xing.modules.sys.dao;

import cn.xing.modules.sys.entity.AfterSaleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 售后记录
 * 
 * @author 
 * @email 
 * @date 2020-11-02 18:54:17
 */
@Mapper
public interface AfterSaleDao extends BaseMapper<AfterSaleEntity> {
	
}
