package cn.xing.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.sys.entity.RefundInfoEntity;

import java.util.Map;

/**
 * 退款信息
 *
 * @author 
 * @email 
 * @date 2020-11-02 18:54:17
 */
public interface RefundInfoService extends IService<RefundInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

