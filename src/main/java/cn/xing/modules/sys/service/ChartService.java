package cn.xing.modules.sys.service;

import cn.xing.enums.ChartTimeUnitEnum;
import cn.xing.modules.sys.vo.OrderCountLineVo;
import cn.xing.modules.sys.vo.SaleCountPieVo;

import java.util.List;

public interface ChartService {
    /**
     * 查询 成交订单数、销售额
     * @return
     * @param timeUnitEnum
     * @param startTime
     * @param endTime
     */
    OrderCountLineVo queryLineUnion(ChartTimeUnitEnum timeUnitEnum, String startTime, String endTime);

    /**
     * 查询 销量数据
     * @return
     * @param startTime
     * @param endTime
     */
    List<SaleCountPieVo> querySaleCountPieDataList(String startTime, String endTime);

}
