package cn.xing.modules.sys.service;

import cn.xing.modules.app.dto.AfterSaleDto;
import cn.xing.modules.app.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.sys.entity.AfterSaleEntity;

import java.util.Map;

/**
 * 售后记录
 *
 * @author 
 * @email 
 * @date 2020-11-02 18:54:17
 */
public interface AfterSaleService extends IService<AfterSaleEntity> {


    /**
     * 发起售后
     * @param afterSaleDto
     * @param user
     */
    void createAfterSale(AfterSaleDto afterSaleDto, UserEntity user);

    AfterSaleEntity queryByOrderSn(String orderSn);

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 拒绝售后
     * @param id
     */
    void refuse(Long id);

    /**
     * 接受售后
     * @param id
     */
    void confirm(Long id);

}

