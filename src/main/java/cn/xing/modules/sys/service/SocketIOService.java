package cn.xing.modules.sys.service;

import java.util.List;

public interface SocketIOService {
    /**
     * 启动服务
     */
    void start();

    /**
     * 停止服务
     */
    void stop();

    /**
     * 推送信息给指定客户端
     *
     * @param userId:     客户端唯一标识
     * @param msgContent: 消息内容
     */
    void pushMessageToUser(String userId, String eventKey, String msgContent);

    /**
     * 广播，除了指定用户
     * @param userIdList 广播，除了指定用户
     */
    void broadCastExcludeUser(List<String> userIdList, String eventKey, String msgContent);



    /**
     * 广播，除了指定用户
     * @param userId 广播，除了指定用户
     */
    void broadCastExcludeUser(String userId, String eventKey, String msgContent);

    /**
     * 广播
     *
     */
    void broadCast(String eventKey, String msgContent);

    void newOrderBroadCast();
}
