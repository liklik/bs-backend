package cn.xing.modules.app.controller;

import cn.xing.common.utils.R;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.app.annotation.LoginUser;
import cn.xing.modules.app.entity.ReceiverAddressEntity;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.app.service.ReceiverAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@Api(description = "APP 收货地址接口")
@RequestMapping("/app/receiveraddress")
public class AppReceiverAddressController {
    @Autowired
    private ReceiverAddressService receiverAddressService;

    @PostMapping("/")
    @Login
    @ApiOperation("新增收货地址")
    public R add(@ApiIgnore @LoginUser UserEntity userEntity, @RequestBody ReceiverAddressEntity receiverAddressEntity) {
        receiverAddressEntity.setUserId(userEntity.getUserId());

        receiverAddressService.save(receiverAddressEntity);
        return R.ok();
    }

    @GetMapping("/all")
    @Login
    @ApiOperation("查询我的所有收货地址")
    public R all(@ApiIgnore @LoginUser UserEntity userEntity) {
        List<ReceiverAddressEntity> list = receiverAddressService.listMyReceiverAddress(userEntity.getUserId());

        return R.ok().put("data", list);
    }

    @PutMapping("/{id}")
    @Login
    @ApiOperation("根据id修改收货地址")
    public R updateMyReceiverAddress(@ApiIgnore @LoginUser UserEntity userEntity,
                                     @RequestBody ReceiverAddressEntity receiverAddressEntity,
                                     @PathVariable("id") Long id) {
        receiverAddressEntity.setUserId(userEntity.getUserId());
        receiverAddressEntity.setId(id);

        receiverAddressService.updateById(receiverAddressEntity);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    @Login
    @ApiOperation("根据id删除收货地址")
    public R deleteMyReceiverAddress(@ApiIgnore @LoginUser UserEntity userEntity,
                                     @PathVariable("id") Long id) {


        receiverAddressService.removeMyReceiverAddressById(id,userEntity.getUserId());
        return R.ok();
    }
}
