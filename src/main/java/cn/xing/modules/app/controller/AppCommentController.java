package cn.xing.modules.app.controller;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.app.annotation.LoginUser;
import cn.xing.modules.app.dto.CommentDto;
import cn.xing.modules.app.entity.CommentEntity;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.app.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * APP评论
 */
@RestController
@RequestMapping("/app/comment")
@Api(description = "APP 评论接口")
public class AppCommentController {
    @Autowired
    private CommentService commentService;

    @Login
    @ApiOperation("新增评论")
    @PostMapping("/")
    public R create(@ApiIgnore @LoginUser UserEntity user,@RequestBody CommentDto commentDto) {
        CommentEntity commentEntity = new CommentEntity();
        BeanUtils.copyProperties(commentDto, commentEntity);
        commentService.save(user,commentEntity);
        return R.ok();
    }

    @Login
    @GetMapping()
    @ApiOperation("以orderSn，来查询评论（1:1）")
    public R queryByOrderSn(String orderSn) {
        CommentEntity commentEntity = commentService.queryByOrderSn(orderSn);
        return R.ok().put("data", commentEntity);
    }

    // todo 设计前端通过 标签（条件筛选）方式查询
    // 最新
    // 好评、差评（通过评分）
    // 有图

    /**
     * 列表
     */
    @ApiOperation("查询 商家的所有评论，分页查询，无需登录。\n 标签：highScore（好评）、lowScore（差评）、hasImage（有图）")
    @GetMapping("/list")
    // @RequiresPermissions("modules:comment:list")
    public R list(String tag,@ApiIgnore @RequestParam Map<String, Object> params) {
        PageUtils page = commentService.queryPage(tag,params);

        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @Login
    @ApiOperation("查询 我的所有评论")
    @GetMapping("/list/mine")
    // @RequiresPermissions("modules:comment:list")
    public R myComment(@ApiIgnore @LoginUser UserEntity user) {

        List<CommentEntity> list= commentService.queryMyCommentPage(user.getUserId());

        return R.ok().put("data", list);
    }
}
