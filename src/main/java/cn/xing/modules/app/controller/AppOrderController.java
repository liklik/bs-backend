package cn.xing.modules.app.controller;

import cn.xing.common.exception.RRException;
import cn.xing.common.utils.R;
import cn.xing.config.AlipayTemplate;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.app.annotation.LoginUser;
import cn.xing.modules.app.dto.OrderDto;
import cn.xing.modules.app.entity.OrderEntity;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.app.service.OrderService;
import cn.xing.modules.app.vo.PayVo;
import com.alipay.api.AlipayApiException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/app/order")
@Api(description = "APP 订单接口")
@Slf4j
public class AppOrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private AlipayTemplate alipayTemplate;

    @ApiOperation("创建订单")
    @PostMapping("/")
    @Login
    public R createOrder(@ApiIgnore @LoginUser UserEntity userEntity, @RequestBody OrderDto orderDto) {

        OrderEntity orderEntity = orderService.createOrderByDto(userEntity, orderDto);

        return R.ok().put("data",orderEntity);
    }

    @ApiOperation("查询我的订单")
    @GetMapping("/all")
    @Login
    public R createOrder(@ApiIgnore @LoginUser UserEntity userEntity) {

        List<OrderEntity> orderEntities = orderService.getByUserId(userEntity.getUserId());

        return R.ok().put("data", orderEntities);
    }


    @ApiOperation("根据orderSn取消我的订单")
    @PutMapping("/{orderSn}/cancel")
    @Login
    public R cancelOrder(@ApiIgnore @LoginUser UserEntity userEntity,
                         @PathVariable("orderSn") String orderSn) {

        orderService.userCancelOrder(userEntity.getUserId(), orderSn);

        return R.ok();
    }

    @ApiOperation("根据orderSn 确认收货")
    @PutMapping("/{orderSn}/receive")
    @Login
    public R userConfirmReceiving(@ApiIgnore @LoginUser UserEntity userEntity,
                         @PathVariable("orderSn") String orderSn) {

        orderService.userConfirmReceiving(userEntity.getUserId(), orderSn);

        return R.ok();
    }

    @ApiOperation("查询订单详情")
    @GetMapping("/{orderSn}")
    @Login
    public R orderInfo(@ApiIgnore @LoginUser UserEntity userEntity
            , @PathVariable("orderSn") String orderSn) {

        OrderEntity orderEntity = orderService.getByOrderSnWithOrderItems(orderSn);
        return R.ok().put("data", orderEntity);
    }


    @ApiOperation("根据orderSn，去支付，支付成功跳转到returnUrl。！！注意这里返回的是html")
    @GetMapping(value = "/pay",produces = {"text/html"})
    public String toPay(@RequestParam("orderSn") String orderSn,
                        @RequestParam("returnUrl")String returnUrl) throws AlipayApiException {
        if(StringUtils.isEmpty(orderSn)){
            throw new RRException("orderSn不能为空");
        }
        PayVo payVo =orderService.getOrderPayVo(orderSn);

        String payPage = alipayTemplate.wapPay(payVo,returnUrl);

        log.info(payVo.toString());
        return payPage;
    }


}
