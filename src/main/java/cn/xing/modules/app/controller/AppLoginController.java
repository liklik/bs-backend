

package cn.xing.modules.app.controller;


import cn.xing.common.constant.RedisConstans;
import cn.xing.common.exception.RRException;
import cn.xing.common.utils.R;
import cn.xing.common.validator.ValidatorUtils;
import cn.xing.modules.app.form.LoginForm;
import cn.xing.modules.app.service.UserService;
import cn.xing.modules.app.service.VerificationService;
import cn.xing.modules.app.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * APP登录授权
 */
@RestController
@RequestMapping("/app")
@Api(description = "APP登录接口")
public class AppLoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private VerificationService verificationService;

    @ApiOperation("获取短信验证码，无需登录")
    @PostMapping("code/sms")
    public R sendLoginSmsCode(String phone) {
        verificationService.sendLoginSmsCode(RedisConstans.CODE_LOGIN_PREFIX,phone);

        return R.ok();
    }

    /**
     * 登录
     */
    @PostMapping("login")
    @ApiOperation("登录")
    public R login(@RequestBody LoginForm form) {
        //
        //表单校验
        ValidatorUtils.validateEntity(form);

        // 验证码校验
        if(!verificationService.checkSmsCode(RedisConstans.CODE_LOGIN_PREFIX, form.getMobile(), form.getCode())){
            throw new RRException("验证码错误");
        }

        //用户登录
        long userId = userService.loginOrRegister(form);

        //生成token
        String token = jwtUtils.generateToken(userId);

        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("expire", jwtUtils.getExpire());

        return R.ok(map);
    }

}
