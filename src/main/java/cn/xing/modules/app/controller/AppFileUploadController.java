package cn.xing.modules.app.controller;

import cn.xing.common.utils.R;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.oss.cloud.CloudStorageService;
import cn.xing.modules.oss.cloud.OSSFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "APP 文件上传")
@RestController
public class AppFileUploadController {

    // @Login
    @PostMapping("app/uploadtoken/qiniu")
    @ApiOperation("获取七牛上传token")
    public R genQiniuUploadToken() {
        String token = "";
        CloudStorageService cloudStorageService = OSSFactory.build();
        if (cloudStorageService != null) {
            token = cloudStorageService.fetchUploadToken();
        }

        return R.ok().put("data", token);
    }
}
