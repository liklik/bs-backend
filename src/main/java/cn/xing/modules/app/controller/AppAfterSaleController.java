package cn.xing.modules.app.controller;

import cn.xing.common.utils.R;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.app.annotation.LoginUser;
import cn.xing.modules.app.dto.AfterSaleDto;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.sys.entity.AfterSaleEntity;
import cn.xing.modules.sys.service.AfterSaleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@Api(description = "APP 售后记录接口")
@RequestMapping("/app/aftersale")
@RestController
public class AppAfterSaleController {
    @Autowired
    private AfterSaleService afterSaleService;
    @Login
    @ApiOperation("申请售后")
    @PostMapping("/")
    public R create(@ApiIgnore @LoginUser UserEntity user, @RequestBody AfterSaleDto afterSaleDto) {
        afterSaleService.createAfterSale(afterSaleDto,user);
        return R.ok();
    }

    @GetMapping()
    @ApiOperation("以orderSn，来查询售后记录（1:1）")
    public R queryByOrderSn(String orderSn) {
       AfterSaleEntity afterSaleEntity=  afterSaleService.queryByOrderSn(orderSn);
       return R.ok().put("data", afterSaleEntity);
    }
}
