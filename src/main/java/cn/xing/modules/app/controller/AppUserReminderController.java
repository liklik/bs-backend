package cn.xing.modules.app.controller;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.R;
import cn.xing.modules.app.annotation.Login;
import cn.xing.modules.app.annotation.LoginUser;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.app.entity.UserReminderEntity;
import cn.xing.modules.app.service.UserReminderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;


/**
 * 用户提醒
 *
 * @author
 * @email
 * @date 2020-11-11 16:29:49
 */
@RestController
@RequestMapping("app/userreminder")
@Api(description = "APP 用户提醒")
public class AppUserReminderController {
    @Autowired
    private UserReminderService userReminderService;

    /**
     * 列表
     */
    @ApiOperation("分页查询,默认只有未读，参数见 表tb_user_reminder（驼峰命名）")
    @GetMapping("/list")
    @Login
    // @RequiresPermissions("sys:userreminder:list")
    public R list(@ApiIgnore @LoginUser UserEntity user, @RequestParam Map<String, Object> params) {

        PageUtils page = userReminderService.queryPage(params,user.getUserId());

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("根据id获取用户提醒详情，并标记为已读")
    @GetMapping("/{id}")
    @Login
    // @RequiresPermissions("sys:userreminder:info")
    public R info(@PathVariable("id") Long id) {
        UserReminderEntity userReminder = userReminderService.markReaded(id);

        return R.ok().put("data", userReminder);
    }

    /**
     * 信息
     */
    @ApiOperation("标记 所有 为已读")
    @PutMapping("/all")
    @Login
    // @RequiresPermissions("sys:userreminder:info")
    public R readAll(@ApiIgnore @LoginUser UserEntity user) {
        userReminderService.markAllReaded(user.getUserId());

        return R.ok();
    }


}
