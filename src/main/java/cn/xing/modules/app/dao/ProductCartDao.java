package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.ProductCartEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 购物车
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Mapper
public interface ProductCartDao extends BaseMapper<ProductCartEntity> {
	
}
