package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 订单项
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {

    List<OrderItemEntity> queryPayed();
}
