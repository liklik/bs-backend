package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.SaleAttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 销售属性
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
@Mapper
public interface SaleAttrDao extends BaseMapper<SaleAttrEntity> {

    List<SaleAttrEntity> getSaleAttrByProductId(@Param("id") Long id);

    List<String> queryDistinctName(@Param("key") String key);

}
