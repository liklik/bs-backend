package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.UserReminderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 用户提醒
 *
 * @author
 * @email
 * @date 2020-11-11 16:29:49
 */
@Mapper
public interface UserReminderDao extends BaseMapper<UserReminderEntity> {

    void markAllReaded(@Param("userId") Long userId, @Param("date") Date date);
}
