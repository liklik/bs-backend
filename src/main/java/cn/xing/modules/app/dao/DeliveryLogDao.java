package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.DeliveryLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 配送记录
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Mapper
public interface DeliveryLogDao extends BaseMapper<DeliveryLogEntity> {
	
}
