package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.ProductCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品类别
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Mapper
public interface ProductCategoryDao extends BaseMapper<ProductCategoryEntity> {
	
}
