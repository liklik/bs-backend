package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.SysNotificationEntity;
import cn.xing.modules.sys.entity.SysUserEntity;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统通知
 *
 * @author
 * @email
 * @date 2020-11-11 16:29:49
 */
@Mapper
public interface SysNotificationDao extends BaseMapper<SysNotificationEntity> {

    List<SysNotificationEntity> listMyUnread(@Param("userId") Long userId, @Param("username") String username);

    IPage<SysNotificationEntity> selectPage(@Param("page") IPage<SysNotificationEntity> page, @Param("queryWrapper") Wrapper<SysNotificationEntity> queryWrapper, @Param("sysUserEntity") SysUserEntity sysUserEntity);

    // @Override
    // <E extends IPage<SysNotificationEntity>> E selectPage(E page, Wrapper<SysNotificationEntity> queryWrapper);
    //
    // IPage<SysNotificationEntity> selectPage(@Param("page") IPage<Object> page, @Param("ew") Wrapper<SysNotificationEntity> queryWrapper);
}
