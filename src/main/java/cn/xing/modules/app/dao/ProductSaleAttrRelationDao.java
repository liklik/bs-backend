package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.ProductSaleAttrRelationEntity;
import cn.xing.modules.app.entity.SysNotificationEntity;
import cn.xing.modules.sys.entity.SysUserEntity;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品-销售属性关系
 *
 * @author
 * @email
 * @date 2020-10-24 21:02:17
 */
@Mapper
public interface ProductSaleAttrRelationDao extends BaseMapper<ProductSaleAttrRelationEntity> {
    IPage<ProductSaleAttrRelationEntity> selectPage(@Param("page") IPage<ProductSaleAttrRelationEntity> page, @Param("key") String key);

}
