package cn.xing.modules.app.dao;

import cn.xing.enums.ChartTimeUnitEnum;
import cn.xing.modules.app.entity.OrderEntity;
import cn.xing.modules.sys.vo.OrderCountLineVo;
import cn.xing.modules.sys.vo.OrderCountVo;
import cn.xing.modules.sys.vo.TurnoverCountVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {

    List<OrderCountVo> queryOrderCountByDay(@Param("startDate") String startDate, @Param("endDate") String endDate);

    List<OrderCountVo> queryOrderCountByMonth(@Param("startDate") String startDate, @Param("endDate") String endDate);


    void updateOrderStatusByOrderSn(@Param("orderSn") String orderSn, @Param("code") Integer code);
}
