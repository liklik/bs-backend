package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.SysNotificationReadLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统通知已读记录
 * 
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
@Mapper
public interface SysNotificationReadLogDao extends BaseMapper<SysNotificationReadLogEntity> {
	
}
