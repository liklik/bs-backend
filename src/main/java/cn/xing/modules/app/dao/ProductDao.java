package cn.xing.modules.app.dao;

import cn.xing.modules.app.entity.ProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
@Mapper
public interface ProductDao extends BaseMapper<ProductEntity> {

    void cumulativeSaleCount(@Param("productId") Long productId, @Param("num") Integer num);
}
