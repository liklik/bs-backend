package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 商品
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Data
@TableName("tb_product")
public class ProductEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 商品名称
	 */
	private String name;
	/**
	 * 主图
	 */
	private String defaultImg;
	/**
	 * 图片
	 */
	private String imgUrls;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 基础价格
	 */
	private BigDecimal basePrice;
	/**
	 * 销量
	 */
	private Long saleCount;
	/**
	 * 商品分类id
	 */
	private Long categoryId;
	/**
	 * 版本号
	 */
	private Long version;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

	@TableField(exist = false)
	private String categoryName;

	/**
	 * 销售属性 列表
	 */
	@TableField(exist = false)
	private List<SaleAttrEntity> saleAttrList;
}
