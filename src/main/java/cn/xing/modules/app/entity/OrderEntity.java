package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
@Data
@TableName("tb_order")
public class OrderEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Long id;
    /**
     * 订单唯一标识
     */
    private String orderSn;

    private Long userId;

    /**
     * 收货人姓名
     */
    private String receiverName;
    /**
     * 收货人详细地址
     */
    private String receiverDetailAddress;
    /**
     * 收货人手机号
     */
    private String receiverPhone;
    /**
     * 订单状态 -1：取消；0：新建,待付款；1：已付款；2：已送出；3：已送达；
     */
    private Integer orderStatus;
    /**
     * 确认收货状态 0：未确认；1：已确认
     */
    private Integer confirmStatus;
    /**
     * 退款状态 0：未退款 1：已退款
     */
    private Integer refundStatus;
    /**
     * 订单备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 实际应付价格
     */
    private BigDecimal realAmount;

    @TableField(exist = false)
    private List<OrderItemEntity> orderItemList;

    @TableField(exist = false)
    private String orderStatusChinese;

    @TableField(exist = false)
    private String orderConfirmStatusChinese;


	@TableField(exist = false)
	private String orderRefundStatusChinese;
}
