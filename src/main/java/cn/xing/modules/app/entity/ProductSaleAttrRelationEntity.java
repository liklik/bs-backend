package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 商品-销售属性关系
 * 
 * @author 
 * @email 
 * @date 2020-10-24 21:02:17
 */
@Data
@TableName("tb_product_sale_attr_relation")
public class ProductSaleAttrRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 商品ID
	 */
	private Long productId;
	/**
	 * 销售属性ID
	 */
	private Long saleAttrId;
	/**
	 * 额外价格
	 */
	private BigDecimal extraPrice;

	/**
	 * 商品名称
	 */
	@TableField(exist = false)
	private String productName;

	/**
	 * 销售属性全名 即 销售属性名-销售属性值
	 */
	@TableField(exist = false)
	private String saleAttrFullName;

	/**
	 * 销售属性名
	 */
	@TableField(exist = false)
	private String saleAttrName;
}
