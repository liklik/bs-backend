package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 销售属性
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Data
@TableName("tb_sale_attr")
public class SaleAttrEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 销售属性名
	 */
	private String name;
	/**
	 * 属性值
	 */
	private String attrValue;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

	/**
	 * 额外价格
	 */
	@TableField(exist = false)
	private BigDecimal extraPrice;
}
