

package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户
 *
 *
 */
@Data
@TableName("tb_user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@TableId
	private Long userId;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 修改时间
	 */
	private Date updateTime;

	public String getSensitiveName(UserEntity user) {
		StringBuilder sensitiveUsername = new StringBuilder("匿名用户");
		String username = user.getUsername();
		String mobile = user.getMobile();
		if (StringUtils.isNotEmpty(username)) {
			char[] chars = username.toCharArray();
			sensitiveUsername = new StringBuilder();
			sensitiveUsername.append(chars[0]).append("***").append(chars[chars.length - 1]);
		}else if(StringUtils.isNotEmpty(mobile)){
			if(mobile.length()>10){
				sensitiveUsername = new StringBuilder();
				sensitiveUsername.append(mobile.substring(1,3))
						.append("****")
						.append(mobile.substring(mobile.length()-4,mobile.length()-1));
			}
		}

		return sensitiveUsername.toString();
	}
}
