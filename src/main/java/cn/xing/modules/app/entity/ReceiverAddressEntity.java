package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 收货地址
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Data
@TableName("tb_receiver_address")
public class ReceiverAddressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 详细地址
	 */
	private String detailAddress;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * 收货人姓名
	 */
	private String name;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
