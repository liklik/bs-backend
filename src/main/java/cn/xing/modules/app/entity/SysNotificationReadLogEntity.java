package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 系统通知已读记录
 * 
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
@Data
@TableName("tb_sys_notification_read_log")
public class SysNotificationReadLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 系统用户id
	 */
	private Long sysUserId;
	/**
	 * 系统通知id
	 */
	private Long sysNotificationId;
	/**
	 * 阅读时间
	 */
	private Date readTime;

}
