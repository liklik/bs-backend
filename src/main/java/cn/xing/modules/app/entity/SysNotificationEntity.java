package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 系统通知
 * 
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
@Data
@TableName("tb_sys_notification")
public class SysNotificationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 管理员用户名
	 */
	private String sysUsername;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 消息类型 0:订单状态改变通知 1:售后状态变更提醒 2:评论提醒
	 */
	private Integer type;
	/**
	 * 参数json
	 */
	private String params;

	private Date createTime;
}
