package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户提醒
 * 
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
@Data
@TableName("tb_user_reminder")
public class UserReminderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 已读 0:未读；1:已读
	 */
	private Integer readed;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 阅读时间
	 */
	private Date readTime;
	/**
	 * 消息类型 -1:初次注册消息 0:订单状态改变通知 1:商家售后状态变更提醒
	 */
	private Integer type;
	/**
	 * 参数json
	 */
	private String params;

	private Date createTime;
}
