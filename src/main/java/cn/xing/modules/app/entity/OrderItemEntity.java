package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 订单项
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Data
@TableName("tb_order_item")
public class OrderItemEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 订单号
	 */
	private String orderSn;
	/**
	 * 商品id
	 */
	private Long productId;
	/**
	 * 商品名称
	 */
	private String productName;
	/**
	 * 分类id
	 */
	private Long categoryId;

	/**
	 * 数量
	 */
	private Integer num;

	/**
	 * 组合的销售属性值
	 */
	private String saleAttrValueArray;
	/**
	 * 基础价格
	 */
	private BigDecimal basePrice;
	/**
	 * 销售属性额外价格总计
	 */
	private BigDecimal saleAttrExtraPrice;
	/**
	 * 优惠
	 */
	private BigDecimal couponAmount;
	/**
	 * 实际应付价格
	 */
	private BigDecimal realAmount;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
