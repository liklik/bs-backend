package cn.xing.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 配送记录
 * 
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
@Data
@TableName("tb_delivery_log")
public class DeliveryLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 订单号
	 */
	private String orderSn;
	/**
	 * 配送开始时间
	 */
	private Date deliveryStartTime;
	/**
	 * 送达时间
	 */
	private Date deliveryEndTime;

}
