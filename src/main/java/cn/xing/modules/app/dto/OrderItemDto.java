package cn.xing.modules.app.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderItemDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    // private Integer id;
    /**
     * 订单号
     */
    // private String orderSn;
    /**
     * 商品id
     */
    private Long productId;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 销售属性  ID 列表
     */
    private List<Long> saleAttrIdList;
    // /**
    //  * 基础价格
    //  */
    // private BigDecimal basePrice;
    // /**
    //  * 销售属性额外价格总计
    //  */
    // private BigDecimal saleAttrExtraPrice;
    // /**
    //  * 优惠
    //  */
    // private BigDecimal couponAmount;
    /**
     * 实际应付价格
     */
    private BigDecimal realAmount;

}
