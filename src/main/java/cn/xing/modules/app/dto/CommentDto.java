package cn.xing.modules.app.dto;

import lombok.Data;

@Data
public class CommentDto {
    /**
     * 内容
     */
    private String content;
    /**
     * 图片链接
     */
    private String imgUrls;
    /**
     * 分值
     */
    private Integer score;

    /**
     * 订单号
     */
    private String orderSn;

}
