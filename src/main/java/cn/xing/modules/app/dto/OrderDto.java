package cn.xing.modules.app.dto;

import cn.xing.modules.app.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderDto {
    /**
     * 收货人姓名
     */
    private String receiverName;
    /**
     * 收货人详细地址
     */
    private String receiverDetailAddress;
    /**
     * 收货人手机号
     */
    private String receiverPhone;

    /**
     * 订单备注
     */
    private String remark;

    /**
     * 订单项
     */
    private List<OrderItemDto> orderItemEntities;

    /**
     * 实际应付价格
     */
    private BigDecimal realAmount;

}
