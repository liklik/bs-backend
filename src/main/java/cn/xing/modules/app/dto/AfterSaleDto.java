package cn.xing.modules.app.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AfterSaleDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 订单号
     */
    private String orderSn;
    /**
     * 用户-原因
     */
    private String reason;
    /**
     * 图片数组，以;分割
     */
    private String imgUrls;

}
