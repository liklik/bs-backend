package cn.xing.modules.app.service.impl;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;
import cn.xing.modules.app.dao.SaleAttrDao;
import cn.xing.modules.app.entity.SaleAttrEntity;
import cn.xing.modules.app.service.ProductSaleAttrRelationService;
import cn.xing.modules.app.service.SaleAttrService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("saleAttrService")
public class SaleAttrServiceImpl extends ServiceImpl<SaleAttrDao, SaleAttrEntity> implements SaleAttrService {

    @Autowired
    private ProductSaleAttrRelationService productSaleAttrRelationService;

    @Override
    public List<SaleAttrEntity> getByName(String name) {

        return this.baseMapper.selectList(new QueryWrapper<SaleAttrEntity>().eq("name", name));
    }

    @Override
    public List<SaleAttrEntity> getByProductId(Long id) {
        return this.baseMapper.getSaleAttrByProductId(id);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");

        IPage<SaleAttrEntity> page = this.page(
                new Query<SaleAttrEntity>().getPage(params),
                new QueryWrapper<SaleAttrEntity>()
                        .like(StringUtils.isNotEmpty(key), "name", key)
                        .or()
                        .like(StringUtils.isNotEmpty(key), "attr_value", key)
                        .or()
                        .like(StringUtils.isNotEmpty(key), "remark", key)

        );

        return new PageUtils(page);
    }

    @Override
    public List<String> queryDistinctName(String key) {

        return this.baseMapper.queryDistinctName(key);
    }


}