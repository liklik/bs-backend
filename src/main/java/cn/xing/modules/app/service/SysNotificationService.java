package cn.xing.modules.app.service;

import cn.xing.modules.app.entity.OrderEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.SysNotificationEntity;

import java.util.List;
import java.util.Map;

/**
 * 系统通知
 *
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
public interface SysNotificationService extends IService<SysNotificationEntity> {

    SysNotificationEntity getAndMarkReaded(Long id);

    /**
     * 列出 我 未读的 通知。
     * 未读 = sysUsername 不是我 && readLog中没有我的记录
     * @return
     */
    List<SysNotificationEntity> listMyUnread();

    /**
     * 标记 所有为 已读
     */
    void markAllRead();

    /**
     * 用户取消订单
     * @param orderEntity
     */
    void onOrderCancel(OrderEntity orderEntity);

    /**
     * 用户 确认收货
     * @param orderSn
     */
    void onOrderConfirm(String orderSn);

    /**
     * 用户申请退款（接单后的，需要商家处理）
     * @param orderEntity
     */
    void onOrderUserCancelAndRefund(OrderEntity orderEntity);

    /**
     * 有售后申请
     * @param orderSn
     */
    void onUserRequireAfterSale(String orderSn);


    PageUtils queryPage(Map<String, Object> params);

    /**
     * 新订单通知
     * @param
     */
    void onOrderPayed(OrderEntity orderEntity);


    PageUtils queryPageUserUnread(Map<String, Object> params);
}

