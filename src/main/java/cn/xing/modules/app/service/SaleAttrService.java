package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.SaleAttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 销售属性
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface SaleAttrService extends IService<SaleAttrEntity> {

    List<SaleAttrEntity> getByName(String name);

    List<SaleAttrEntity> getByProductId(Long id);

    PageUtils queryPage(Map<String, Object> params);


    List<String> queryDistinctName(String key);

}

