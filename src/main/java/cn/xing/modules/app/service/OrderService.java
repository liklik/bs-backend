package cn.xing.modules.app.service;

import cn.xing.common.utils.PageUtils;
import cn.xing.enums.ChartTimeUnitEnum;
import cn.xing.modules.app.dto.OrderDto;
import cn.xing.modules.app.dto.PayAsyncDto;
import cn.xing.modules.app.entity.OrderEntity;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.app.vo.PayVo;
import cn.xing.modules.sys.vo.OrderCountVo;
import com.alipay.api.AlipayApiException;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 订单
 *
 * @author
 * @email
 * @date 2020-10-19 16:19:08
 */
public interface OrderService extends IService<OrderEntity> {

    /**
     * 商家拒绝订单
     *
     * @param id
     * @param operatorId
     */
    void managerRefuseOrder(Long id, Long operatorId);


    /**
     * 商家配送订单
     *
     * @param id
     * @param operatorId
     */
    void managerDeliveryOrder(Long id, Long operatorId);

    /**
     * 商家确认订单送达
     *
     * @param id
     * @param operatorId
     */
    void managerConfirmSendedOrder(Long id, Long operatorId);

    /**
     * 商家接单
     *
     * @param id
     * @param operatorId
     */
    void managerAcceptOrder(Long id, Long operatorId);

    /**
     * 商家取消订单
     *
     * @param operatorId
     * @param id
     */
    void managerCancelOrder(Long id, Long operatorId);

    /**
     * 查询送出 及以后的
     *
     * @return
     * @param startTime
     * @param endTime
     */
    List<OrderEntity> queryPayed(String startTime, String endTime);




    /**
     * 用户 取消订单
     *
     * @param userId
     * @param orderSn
     */
    void userCancelOrder(Long userId, String orderSn);


    /**
     * 退款，并标记订单为已退款
     *
     * @param orderEntity
     */
    @Transactional(rollbackFor = Exception.class)
    void doRefundAndMark(OrderEntity orderEntity, String reason) throws AlipayApiException;

    @Transactional(rollbackFor = Exception.class)
    void doRefundAndMark(String orderSn, String reason);

    void doUpdateOrderStatus(String orderSn, Integer code);

    OrderEntity getByOrderSn(String orderSn);

    /**
     * 根据dto 创建订单
     *
     * @param userEntity
     * @param orderDto
     * @return
     */
    OrderEntity createOrderByDto(UserEntity userEntity, OrderDto orderDto);

    /**
     * 根据 orderSn 查询订单，
     * 并查询订单项
     *
     * @param orderSn
     * @return
     */
    OrderEntity getByOrderSnWithOrderItems(String orderSn);


    /**
     * 根据用户id 获取订单列表
     *
     * @param userId
     * @return
     */
    List<OrderEntity> getByUserId(Long userId);

    PayVo getOrderPayVo(String orderSn);

    /**
     * 获取订单展示 主题
     *
     * @param orderEntityWithItems
     * @return
     */
    String buildOrderSubject(OrderEntity orderEntityWithItems);

    String buildOrderSubject(String orderSn);

    /**
     * 处理结果
     *
     * @param payAsyncDto
     * @return
     */
    String handlePayResult(PayAsyncDto payAsyncDto);


    PageUtils queryPage(Map<String, Object> params);


    /**
     * 用户确认收货
     *
     * @param userId
     * @param orderSn
     */
    void userConfirmReceiving(Long userId, String orderSn);

    void checkIllegal(OrderEntity orderEntity, Long userId);

    void checkIllegal(String orderSn, Long userId);

    /**
     * 判断是否 可以申请售后
     * （主要是订单状态要 已送达）
     *
     * @param orderSn
     */
    void checkSatisfyAfterSale(String orderSn, Long userId);

    /**
     * 自动接受 已付款的订单
     */
    void ifAutoAcceptPayedOrder(String orderSn);


    /**
     * 根据时间单位，查询营业额、订单
     * @param chartTimeUnitEnum
     * @param startTime
     * @param endTime
     * @return
     */
    List<OrderCountVo> queryOrderCountByTimeUnit(ChartTimeUnitEnum chartTimeUnitEnum, String startTime, String endTime);
}

