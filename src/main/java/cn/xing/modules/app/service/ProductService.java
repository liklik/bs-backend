package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.ProductEntity;

import java.util.Map;

/**
 * 商品
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface ProductService extends IService<ProductEntity> {

    /**
     * 累加 商品销量
     * @param productId
     * @param num
     */
    void cumulativeSaleCount(Long productId, Integer num);

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageWithSaleAttr(Map<String, Object> params);
}

