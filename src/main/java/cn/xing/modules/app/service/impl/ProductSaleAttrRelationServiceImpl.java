package cn.xing.modules.app.service.impl;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;
import cn.xing.modules.app.dao.ProductSaleAttrRelationDao;
import cn.xing.modules.app.entity.ProductEntity;
import cn.xing.modules.app.entity.ProductSaleAttrRelationEntity;
import cn.xing.modules.app.entity.SaleAttrEntity;
import cn.xing.modules.app.entity.SysNotificationEntity;
import cn.xing.modules.app.service.ProductSaleAttrRelationService;
import cn.xing.modules.app.service.ProductService;
import cn.xing.modules.app.service.SaleAttrService;
import cn.xing.modules.sys.entity.SysUserEntity;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("productSaleAttrRelationService")
public class ProductSaleAttrRelationServiceImpl extends ServiceImpl<ProductSaleAttrRelationDao, ProductSaleAttrRelationEntity> implements ProductSaleAttrRelationService {
    @Autowired
    private ProductService productService;
    @Autowired
    private SaleAttrService saleAttrService;

    @Override
    public ProductSaleAttrRelationEntity getByProductIdAndSaleAttrId(Long productId, Long saleAttrId) {
        ProductSaleAttrRelationEntity productSaleAttrRelationEntity = this.baseMapper.selectOne(new QueryWrapper<ProductSaleAttrRelationEntity>().eq("product_id", productId).eq("sale_attr_id", saleAttrId));
        if (productSaleAttrRelationEntity != null) {
            return getById(productSaleAttrRelationEntity.getId());
        } else {
            return productSaleAttrRelationEntity;
        }
    }

    @Override
    public ProductSaleAttrRelationEntity queryByProductIdAndSaleAttrId(Long productId, Long saleAttrId) {

        return this.baseMapper.selectOne(new QueryWrapper<ProductSaleAttrRelationEntity>()
                .eq("product_id", productId)
                .eq("sale_attr_id", saleAttrId));
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");

        IPage<ProductSaleAttrRelationEntity> page = this.page(
                new Query<ProductSaleAttrRelationEntity>().getPage(params),
                key
        );

        List<ProductSaleAttrRelationEntity> collect = page.getRecords().stream().map(item -> {
            return getById(item);
        }).collect(Collectors.toList());

        page.setRecords(collect);

        return new PageUtils(page);
    }

    @Override
    public void saveByProductIdAndSaleAttrId(ProductSaleAttrRelationEntity productSaleAttrRelation) {
        // 其实就是 saveOrUpdate
        ProductSaleAttrRelationEntity attrRelationEntity = queryByProductIdAndSaleAttrId(productSaleAttrRelation.getProductId(), productSaleAttrRelation.getSaleAttrId());
        if (attrRelationEntity == null) {
            this.baseMapper.insert(productSaleAttrRelation);
        } else {
            // 前端只有变更 ExtraPrice
            attrRelationEntity.setExtraPrice(productSaleAttrRelation.getExtraPrice());
            this.baseMapper.updateById(attrRelationEntity);
        }
    }

    IPage<ProductSaleAttrRelationEntity>  page(IPage<ProductSaleAttrRelationEntity>  page,String key) {

        return getBaseMapper().selectPage(page, key);
    }

    @Override
    public ProductSaleAttrRelationEntity getById(Serializable id) {
        ProductSaleAttrRelationEntity item = this.baseMapper.selectById(id);
        if (item != null) {
            ProductEntity productEntity = productService.getById(item.getProductId());
            SaleAttrEntity saleAttrEntity = saleAttrService.getById(item.getSaleAttrId());

            if (productEntity != null) {
                item.setProductName(productEntity.getName());
            }
            if (saleAttrEntity != null) {
                item.setSaleAttrFullName(saleAttrEntity.getName() + "-" + saleAttrEntity.getAttrValue());

                item.setSaleAttrName(saleAttrEntity.getName());
            }
        }
        return item;
    }
}