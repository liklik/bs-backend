package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.ProductCategoryEntity;

import java.util.Map;

/**
 * 商品类别
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface ProductCategoryService extends IService<ProductCategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

