package cn.xing.modules.app.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;

import cn.xing.modules.app.dao.ProductCategoryDao;
import cn.xing.modules.app.entity.ProductCategoryEntity;
import cn.xing.modules.app.service.ProductCategoryService;


@Service("productCategoryService")
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryDao, ProductCategoryEntity> implements ProductCategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");

        IPage<ProductCategoryEntity> page = this.page(
                new Query<ProductCategoryEntity>().getPage(params),
                new QueryWrapper<ProductCategoryEntity>()
                        .like(StringUtils.isNotEmpty(key), "name", key)
        );

        return new PageUtils(page);
    }

}