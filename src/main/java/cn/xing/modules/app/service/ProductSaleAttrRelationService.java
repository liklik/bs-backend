package cn.xing.modules.app.service;

import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.ProductSaleAttrRelationEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 商品-销售属性关系
 *
 * @author
 * @email
 * @date 2020-10-24 21:02:17
 */
public interface ProductSaleAttrRelationService extends IService<ProductSaleAttrRelationEntity> {


    /**
     * 根据 商品id 、 销售属性id 查询 商品-销售属性关系
     * @param productId
     * @param saleAttrId
     * @return
     */
    ProductSaleAttrRelationEntity getByProductIdAndSaleAttrId(Long productId, Long saleAttrId);

    ProductSaleAttrRelationEntity queryByProductIdAndSaleAttrId(Long productId, Long saleAttrId);

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存/更新 商品-销售属性
     * 以productId、SaleAttrId为 联合uk
     * @param productSaleAttrRelation
     */
    void saveByProductIdAndSaleAttrId(ProductSaleAttrRelationEntity productSaleAttrRelation);
}

