package cn.xing.modules.app.service;

import cn.xing.modules.app.entity.OrderEntity;
import cn.xing.modules.sys.entity.AfterSaleEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.UserReminderEntity;

import java.util.Map;

/**
 * 用户提醒
 *
 * @author
 * @email
 * @date 2020-11-11 16:29:49
 */
public interface UserReminderService extends IService<UserReminderEntity> {

    /**
     * 标记所有通知为已读
     * @param userId
     */
    void markAllReaded(Long userId);


    /**
     * 根据id获取用户提醒详情，并标记为已读
     * @param id
     * @return
     */
    UserReminderEntity markReaded(Long id);

    /**
     * 售后 确认退款
     * @param afterSaleEntity
     */
    void onAfterSaleConfirm(AfterSaleEntity afterSaleEntity);

    /**
     * 售后 拒绝
      * @param afterSaleEntity
     */
    void onAfterSaleRefuse(AfterSaleEntity afterSaleEntity);

    /**
     * 商家取消订单后，产生的消息
     * @param orderEntity
     */
    void onOrderCancel(OrderEntity orderEntity);

    void onOrderRefunded(OrderEntity orderEntity);

    void onOrderSendOut(OrderEntity orderEntity);

    void onOrderSended(OrderEntity orderEntity);

    void onOrderTaken(OrderEntity orderEntity);


    PageUtils queryPage(Map<String, Object> params, Long userId);
}

