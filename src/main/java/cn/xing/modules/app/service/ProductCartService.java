package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.ProductCartEntity;

import java.util.Map;

/**
 * 购物车
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface ProductCartService extends IService<ProductCartEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

