package cn.xing.modules.app.service.impl;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;
import cn.xing.modules.app.dao.ProductDao;
import cn.xing.modules.app.entity.ProductCategoryEntity;
import cn.xing.modules.app.entity.ProductEntity;
import cn.xing.modules.app.entity.SaleAttrEntity;
import cn.xing.modules.app.service.ProductCategoryService;
import cn.xing.modules.app.service.ProductService;
import cn.xing.modules.app.service.SaleAttrService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("productService")
public class ProductServiceImpl extends ServiceImpl<ProductDao, ProductEntity> implements ProductService {
    @Autowired
    private ProductCategoryService productCategoryService;

    @Autowired
    private SaleAttrService saleAttrService;

    @Override
    public void cumulativeSaleCount(Long productId, Integer num) {
        this.baseMapper.cumulativeSaleCount(productId, num);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        String saleCountOrder = (String) params.getOrDefault("saleCountOrder", "");
        QueryWrapper<ProductEntity> queryWrapper = new QueryWrapper<ProductEntity>()
                .like(StringUtils.isNotEmpty(key), "name", key);
        if ("null".equals(saleCountOrder)) {
            // 无排序

        } else if ("ascending".equals(saleCountOrder)) {
            queryWrapper.orderByAsc("sale_count");
        } else if ("descending".equals(saleCountOrder)) {
            queryWrapper.orderByDesc("sale_count");
        }

        IPage<ProductEntity> page = this.page(
                new Query<ProductEntity>().getPage(params),
                queryWrapper
        );

        List<ProductEntity> collect = page.getRecords().stream().map((productEntity -> {
            // 根据分类id查出名称
            ProductCategoryEntity byId = productCategoryService.getById(productEntity.getCategoryId());
            productEntity.setCategoryName(byId.getName());

            return productEntity;
        })).collect(Collectors.toList());

        page.setRecords(collect);
        return new PageUtils(page);
    }

    /**
     * 查询 商品信息分页 （连带销售属性），
     * 并根据分类id排序
     *
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageWithSaleAttr(Map<String, Object> params) {
        IPage<ProductEntity> page = this.page(
                new Query<ProductEntity>().getPage(params),
                new QueryWrapper<ProductEntity>().orderByAsc("category_id")
        );

        List<ProductEntity> collect = page.getRecords().stream().map((productEntity -> {
            // ProductWithSaleAttrVo target = new ProductWithSaleAttrVo();
            // BeanUtils.copyProperties(productEntity, target);

            // 根据分类id查出名称
            ProductCategoryEntity byId = productCategoryService.getById(productEntity.getCategoryId());
            productEntity.setCategoryName(byId.getName());

            // 查询 该商品 所有销售属性
            List<SaleAttrEntity> saleAttrList = saleAttrService.getByProductId(productEntity.getId());
            productEntity.setSaleAttrList(saleAttrList);
            return productEntity;

        })).collect(Collectors.toList());

        page.setRecords(collect);
        return new PageUtils(page);
    }


}