package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.SysNotificationReadLogEntity;

import java.util.Map;

/**
 * 系统通知已读记录
 *
 * @author 
 * @email 
 * @date 2020-11-11 16:29:49
 */
public interface SysNotificationReadLogService extends IService<SysNotificationReadLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

