package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.ReceiverAddressEntity;

import java.util.List;
import java.util.Map;

/**
 * 收货地址
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface ReceiverAddressService extends IService<ReceiverAddressEntity> {

    /**
     * 查询我的所有收货地址
     * @param userId
     * @return
     */
    List<ReceiverAddressEntity> listMyReceiverAddress(Long userId);

    PageUtils queryPage(Map<String, Object> params);

    void removeMyReceiverAddressById(Long id, Long userId);
}

