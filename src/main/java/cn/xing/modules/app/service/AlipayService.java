package cn.xing.modules.app.service;

import com.alipay.api.AlipayApiException;

/**
 * 支付宝
 */
public interface AlipayService {
    /**
     * 退款
     * @param orderSn
     */
    void refund(String orderSn, String reason) throws AlipayApiException;

}
