package cn.xing.modules.app.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;

import cn.xing.modules.app.dao.ProductCartDao;
import cn.xing.modules.app.entity.ProductCartEntity;
import cn.xing.modules.app.service.ProductCartService;


@Service("productCartService")
public class ProductCartServiceImpl extends ServiceImpl<ProductCartDao, ProductCartEntity> implements ProductCartService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductCartEntity> page = this.page(
                new Query<ProductCartEntity>().getPage(params),
                new QueryWrapper<ProductCartEntity>()
        );

        return new PageUtils(page);
    }

}