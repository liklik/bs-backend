package cn.xing.modules.app.service.impl;

import cn.xing.common.exception.RRException;
import cn.xing.config.AlipayTemplate;
import cn.xing.modules.app.service.AlipayService;
import com.alipay.api.AlipayApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("alipayService")
public class AlipayServiceImpl implements AlipayService {
    @Autowired
    private AlipayTemplate alipayTemplate;

    @Override
    public void refund(String orderSn, String reason) throws AlipayApiException {
        log.info("支付宝退款orderSn:[{}]", orderSn);

        if (!alipayTemplate.refund(orderSn, reason)) {
            throw new RRException(String.format("支付宝退款失败orderSn:[%s]", orderSn));
        }
    }

}
