package cn.xing.modules.app.service.impl;

import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;
import cn.xing.modules.app.dao.PaymentInfoDao;
import cn.xing.modules.app.entity.PaymentInfoEntity;
import cn.xing.modules.app.service.PaymentInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("paymentInfoService")
public class PaymentInfoServiceImpl extends ServiceImpl<PaymentInfoDao, PaymentInfoEntity> implements PaymentInfoService {

    @Override
    public PaymentInfoEntity queryByOrderSn(String orderSn) {
        return this.baseMapper.selectOne(new QueryWrapper<PaymentInfoEntity>().eq("order_sn", orderSn));
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");

        IPage<PaymentInfoEntity> page = this.page(
                new Query<PaymentInfoEntity>().getPage(params),
                new QueryWrapper<PaymentInfoEntity>()
                        .orderByDesc("create_time")
                        .eq(StringUtils.isNotEmpty(key), "order_sn", key)
                        .or()
                        .like(StringUtils.isNotEmpty(key), "alipay_trade_no", key)
                        .or()
                        .like(StringUtils.isNotEmpty(key), "subject", key)

        );

        return new PageUtils(page);
    }

}