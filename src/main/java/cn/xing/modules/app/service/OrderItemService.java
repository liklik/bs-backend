package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.OrderItemEntity;

import java.util.List;
import java.util.Map;

/**
 * 订单项
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface OrderItemService extends IService<OrderItemEntity> {

    /**
     * 更新商品销量
     */

    void cumulativeSaleCountByOrderSn(String orderSn);

    /**
     * 查询订单项
     * @param orderSn
     * @return
     */
    List<OrderItemEntity> queryByOrderSn(String orderSn);

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询 订单状态为 送出 以后的订单项
     * @return
     */
    List<OrderItemEntity> querySendOut();

}

