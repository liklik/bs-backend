package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.PaymentInfoEntity;

import java.util.Map;

/**
 * 支付信息表
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface PaymentInfoService extends IService<PaymentInfoEntity> {

    PaymentInfoEntity queryByOrderSn(String orderSn);

    PageUtils queryPage(Map<String, Object> params);
}

