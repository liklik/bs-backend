package cn.xing.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.DeliveryLogEntity;

import java.util.Map;

/**
 * 配送记录
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface DeliveryLogService extends IService<DeliveryLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 送达
     * @param orderSn
     */
    void sended(String orderSn);

    /**
     * 开始配送
     * @param orderSn
     */
    void start(String orderSn);
}

