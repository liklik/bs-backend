package cn.xing.modules.app.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;

import cn.xing.modules.app.dao.SysNotificationReadLogDao;
import cn.xing.modules.app.entity.SysNotificationReadLogEntity;
import cn.xing.modules.app.service.SysNotificationReadLogService;


@Service("sysNotificationReadLogService")
public class SysNotificationReadLogServiceImpl extends ServiceImpl<SysNotificationReadLogDao, SysNotificationReadLogEntity> implements SysNotificationReadLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysNotificationReadLogEntity> page = this.page(
                new Query<SysNotificationReadLogEntity>().getPage(params),
                new QueryWrapper<SysNotificationReadLogEntity>()
        );

        return new PageUtils(page);
    }

}