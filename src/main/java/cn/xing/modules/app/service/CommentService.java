package cn.xing.modules.app.service;

import cn.xing.modules.app.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.CommentEntity;

import java.util.List;
import java.util.Map;

/**
 * 评论
 *
 * @author 
 * @email 
 * @date 2020-10-19 16:19:08
 */
public interface CommentService extends IService<CommentEntity> {

    CommentEntity queryByOrderSn(String orderSn);

    /**
     * 查询我的评论
     * @return
     * @param userId
     */
    List<CommentEntity> queryMyCommentPage(Long userId);


    PageUtils queryPage(String tag, Map<String, Object> params);


    void save(UserEntity user, CommentEntity commentEntity);
}

