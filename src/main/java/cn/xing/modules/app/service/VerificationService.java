package cn.xing.modules.app.service;

public interface VerificationService {
    void sendLoginSmsCode(String prefix,String phone);


    boolean checkSmsCode(String prefix, String phone, String code);
}
