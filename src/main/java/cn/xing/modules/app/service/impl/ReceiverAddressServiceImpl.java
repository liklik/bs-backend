package cn.xing.modules.app.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;

import cn.xing.modules.app.dao.ReceiverAddressDao;
import cn.xing.modules.app.entity.ReceiverAddressEntity;
import cn.xing.modules.app.service.ReceiverAddressService;


@Service("receiverAddressService")
public class ReceiverAddressServiceImpl extends ServiceImpl<ReceiverAddressDao, ReceiverAddressEntity> implements ReceiverAddressService {

    @Override
    public List<ReceiverAddressEntity> listMyReceiverAddress(Long userId) {
        return this.baseMapper.selectList(
                new QueryWrapper<ReceiverAddressEntity>()
                        .eq("user_id",userId)
                        .orderByDesc("create_time")

        );
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReceiverAddressEntity> page = this.page(
                new Query<ReceiverAddressEntity>().getPage(params),
                new QueryWrapper<ReceiverAddressEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void removeMyReceiverAddressById(Long id, Long userId) {

        ReceiverAddressEntity receiverAddressEntity = this.baseMapper.selectById(id);
        // 如果是自己的地址
        if(receiverAddressEntity.getUserId().equals(userId)){
            this.baseMapper.deleteById(id);
        }
    }

}