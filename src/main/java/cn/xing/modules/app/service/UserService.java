/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package cn.xing.modules.app.service;


import cn.xing.common.utils.PageUtils;
import cn.xing.modules.app.entity.UserEntity;
import cn.xing.modules.app.form.LoginForm;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 用户
 *
 *
 */
public interface UserService extends IService<UserEntity> {

    UserEntity queryByMobile(String mobile);

    /**
     * 用户登录
     * @param form    登录表单
     * @return 返回用户ID
     */
    long loginOrRegister(LoginForm form);

    PageUtils queryPage(Map<String, Object> params);
}
