package cn.xing.modules.app.service.impl;

import cn.xing.modules.app.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xing.common.utils.PageUtils;
import cn.xing.common.utils.Query;

import cn.xing.modules.app.dao.OrderItemDao;
import cn.xing.modules.app.entity.OrderItemEntity;
import cn.xing.modules.app.service.OrderItemService;
import org.springframework.transaction.annotation.Transactional;


@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {


    @Autowired
    private ProductService productService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void cumulativeSaleCountByOrderSn(String orderSn) {
        List<OrderItemEntity> orderItemEntities = queryByOrderSn(orderSn);
        if(orderItemEntities!=null && orderItemEntities.size()>0){
            for (OrderItemEntity orderItemEntity : orderItemEntities) {
                productService.cumulativeSaleCount(orderItemEntity.getProductId(),orderItemEntity.getNum());
            }
        }
    }

    @Override
    public List<OrderItemEntity> queryByOrderSn(String orderSn) {
        return this.baseMapper.selectList(new QueryWrapper<OrderItemEntity>().eq("order_sn", orderSn));
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<OrderItemEntity> querySendOut() {
        return this.baseMapper.queryPayed();
    }


}